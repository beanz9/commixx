class Collection < ActiveRecord::Base
  belongs_to :user, :class_name => "Spree::User"
  has_many :collection_items
end
