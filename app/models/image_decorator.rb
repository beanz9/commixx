Spree::Image.class_eval do
	attachment_definitions[:attachment][:styles] = {
    	:mini => '75.8x96.8>', # thumbs under image
    	:small => '100x100>', # images on category view
    	:product => '304x381>', # full product image
    	:large => '550x690>' # light box image
  	}
end