Spree::OrderContents.class_eval do
  def add(variant, quantity = 1, options = {}, collection_owner_id)
    timestamp = Time.now
    line_item = add_to_line_item(variant, quantity, options, collection_owner_id)
    options[:line_item_created] = true if timestamp <= line_item.created_at
    after_add_or_remove(line_item, options)
  end

  def add_to_line_item(variant, quantity, options = {}, collection_owner_id)
    line_item = grab_line_item_by_variant(variant, false, options)

    if line_item
      line_item.quantity += quantity.to_i
      line_item.currency = currency unless currency.nil?
      line_item.collection_owner_id = collection_owner_id unless collection_owner_id
    else
      # Spree::PermittedAttributes.line_item_attributes << :collection_user_id
      opts = { currency: order.currency }.merge ActionController::Parameters.new(options).
                                          permit([:id, :variant_id, :quantity, :collection_owner_id])
      line_item = order.line_items.new(quantity: quantity,
                                        variant: variant,
                                        options: opts,
                                        collection_owner_id: collection_owner_id
                                        )
    end
    line_item.target_shipment = options[:shipment] if options.has_key? :shipment
    line_item.save!
    line_item
  end
end
