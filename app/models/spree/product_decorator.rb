Spree::Product.class_eval do
  has_and_belongs_to_many :collections
  # default_scope { where(is_open: true) }
  add_simple_scopes ([
       :descend_by_created_at
  ])

  add_search_scope :in_taxon do |taxon|

    includes(:classifications).
    where("spree_products_taxons.taxon_id" => taxon.self_and_descendants.pluck(:id))
  end
end
