class SiteUrl < ActiveRecord::Base
  validates :taxon_id, presence: true
  validates :url, presence: true

  belongs_to :site

end
