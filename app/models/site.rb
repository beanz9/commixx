class Site < ActiveRecord::Base
  validates :name, presence: true
  validates :url, presence: true

  has_many :site_url, :dependent => :destroy
end
