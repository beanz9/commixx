Spree.ready ($) ->
  if ($ 'form#update-cart').is('*')
    ($ 'form#update-cart a.delete').show().one 'click', ->
      ($ this).parent().parent().find('input.line_item_quantity').val 0
      ($ 'form#update-cart').submit()
      false

  if ($ 'form#update-cart-side').is('*')
    ($ 'form#update-cart-side a.delete').show().one 'click', ->
      ($ this).next().val 0
      ($ 'form#update-cart-side').submit()
      false

  ($ 'form#update-cart').submit ->
    ($ 'form#update-cart #update-button').attr('disabled', true)

Spree.fetch_cart = ->
  $.ajax
    url: Spree.pathFor("cart_link"),
    success: (data) ->
      $('#link-to-cart').html data
