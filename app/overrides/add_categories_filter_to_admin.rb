Deface::Override.new(
  virtual_path: 'spree/admin/featured_products/index',
  name: 'categories_filter',
  insert_bottom: '[data-hook="admin_products_index_search"]',
  :partial => 'spree/admin/shared/categories_filter'
)
