Deface::Override.new(
  virtual_path: 'spree/admin/shared/sub_menu/_product',
  name: 'featured_products_tab',
  insert_bottom: '[data-hook="admin_product_sub_tabs"]',
  text: '<%= tab(:featured_products, label: "featured_products") %>'
)
