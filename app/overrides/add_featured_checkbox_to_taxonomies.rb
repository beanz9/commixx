Deface::Override.new(:virtual_path => 'spree/admin/taxonomies/_form',
					 :name => 'add_featured_checkbox_to_taxonomies_edit',
					 :insert_after => "[data-hook='admin_inside_taxonomy_form']",
					 :partial => "spree/admin/taxonomies/featured_checkbox")
