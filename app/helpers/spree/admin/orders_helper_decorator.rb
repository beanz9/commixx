Spree::Admin::OrdersHelper.module_eval do
  def event_links
    links = []
    @order_events.sort.each do |event|
      if @order.send("can_#{event}?")
        event_txt = ""
        if event == 'approve'
          event_txt = '승인'
        elsif event == 'cancel'
          event_txt = '취소'
        elsif event == 'resume'
          event_txt = '재시작'
        end

        links << button_link_to(Spree.t(event_txt).capitalize, [event, :admin, @order],
                                :method => :put,
                                :icon => "#{event}",
                                :data => { :confirm => Spree.t(:order_sure_want_to, :event => Spree.t(event)) })
      end
    end
    links.join(' ').html_safe
  end
end
