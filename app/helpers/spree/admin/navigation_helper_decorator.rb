Spree::Admin::NavigationHelper.module_eval do
  def tab(*args)
    options = { label: args.first.to_s }

    # Return if resource is found and user is not allowed to :admin
    return '' if klass = klass_for(options[:label]) and cannot?(:admin, klass)

    if args.last.is_a?(Hash)
      options = options.merge(args.pop)
    end
    options[:route] ||=  "admin_#{args.first}"

    destination_url = options[:url] || spree.send("#{options[:route]}_path")
    titleized_label = Spree.t(options[:label], default: options[:label], scope: [:admin, :tab]).titleize

    if titleized_label == 'Taxons'
      return
    end

    if titleized_label == 'Orders'
      titleized_label = '주문'
    elsif titleized_label == 'Products'
      titleized_label = '상품'
    elsif titleized_label == 'Option Types'
      titleized_label = '옵션 타입'
    elsif titleized_label == 'Properties'
      titleized_label = '속성'
    elsif titleized_label == 'Prototypes'
      titleized_label = '프로토타입'
    elsif titleized_label == 'Taxonomies'
      titleized_label = '카테고리'
    elsif titleized_label == 'Taxons'
      titleized_label = '분류'
    elsif titleized_label == 'Relation Types'
      titleized_label = '연관상품 타입'
    elsif titleized_label == 'Reviews'
      titleized_label = '리뷰'
    elsif titleized_label == 'Featured Products'
      titleized_label = '피쳐드 상품'
    elsif titleized_label == 'Sites'
      titleized_label = '사이트'
    elsif titleized_label == 'Scrap Histories'
      titleized_label = '스크랩 이력'
    elsif titleized_label == 'Reports'
      titleized_label = '레포트'
    elsif titleized_label == 'Promotions'
      titleized_label = '프로모션'
    elsif titleized_label == 'Promotion Categories'
      titleized_label = '프로모션 카테고리'
    elsif titleized_label == 'Users'
      titleized_label = '사용자'
    elsif titleized_label == 'Site Regist'
      titleized_label = '사이트 스크랩 요청'
    end


    css_classes = ['sidebar-menu-item']

    if options[:icon]
      link = link_to_with_icon(options[:icon], titleized_label, destination_url)
    else
      link = link_to(titleized_label, destination_url)
    end

    selected = if options[:match_path].is_a? Regexp
      request.fullpath =~ options[:match_path]
    elsif options[:match_path]
      request.fullpath.starts_with?("#{spree.admin_path}#{options[:match_path]}")
    else
      args.include?(controller.controller_name.to_sym)
    end
    css_classes << 'selected' if selected

    if options[:css_class]
      css_classes << options[:css_class]
    end
    content_tag('li', link, class: css_classes.join(' '))
  end
end
