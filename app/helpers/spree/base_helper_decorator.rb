Spree::BaseHelper.module_eval do
  def get_taxonomies
		Spree::Taxonomy.includes(:root => :children).where(:collection => false, :featured_collection => false)
	end

  def get_collections
    Spree::Taxonomy.where(:collection => true).limit(8)
  end

  def get_featurd_collections
    Spree::Taxonomy.where(:featured_collection => true).limit(3)
  end
end
