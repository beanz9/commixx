Spree::FrontendHelper.module_eval do

	def flash_messages(opts = {})
      	ignore_types = ["order_completed"].concat(Array(opts[:ignore_types]).map(&:to_s) || [])

      	html = ""

      	flash.each do |msg_type, text|
        	unless ignore_types.include?(msg_type)
          		if msg_type == 'errors'
          			html << "<div class='message message-error'>"
          			html << "	<h3><i class='fa fa-warning'></i>#{Spree.t(:errors_prohibited_this_record_from_being_saved, :count => flash[:errors].count) }:</h3>"
          			html << "	<p>#{ Spree.t(:there_were_problems_with_the_following_fields) }:</p>"
          			html << "	<ul>"
          			text.each do |t|
          				html << "<li>#{ t }</li>"
          			end
          			html << "	</ul>"
          			html << "	<br/>"
          			html << "	<button>X</button>"

          			html << "</div>"

          		elsif msg_type == 'notice'
          			html << "<div class='message message-success'>"
          			html << "	<i class='fa fa-thumbs-o-up'></i>#{text}"
          			html << "	<button>X</button>"
          			html << "</div>"
          		end

          		return html.html_safe
        	end
      	end
      	nil
    end

		def sort_types
			sort_options = {
				"Most Popular" => "descend_by_popularity",
				"Name: A to Z" => "ascend_by_name",
				"Name: Z to A" => "descend_by_name",
				"Price: High to Low" => "descend_by_master_price",
				"Price: Low to High" => "ascend_by_master_price",
				"Newest" => "descend_by_created_at"
			}
end


end
