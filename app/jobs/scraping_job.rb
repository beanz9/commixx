require "#{Rails.root}/lib/jobs/scraping"

class ScrapingJob < ActiveJob::Base
  queue_as :default

  def perform(id)
    @scrap_history = ScrapHistory.create(site_id: id, result_status: 0, start_date: Time.now )
    # Do something later
    Scraping.scrap(id)


    # raise StandardError.new("test")

    @scrap_history.update(result_status: 1, end_date: Time.now)
  end

 rescue_from(StandardError) do |exception|
   p exception.backtrace
   @scrap_history.update(result_status: 2, end_date: Time.now, result_desc: exception.backtrace)
 end

end
