Spree::TaxonsController.class_eval do

  helper_method :sorting_param
  alias_method :old_show, :show

  def show
    old_show
    @products = @products.where(is_open: true)
    @products = @products.send(sorting_scope)
  end



  def sorting_param
    params[:sorting].try(:to_sym) || default_sorting
  end

  private

  def sorting_scope
    allowed_sortings.include?(sorting_param) ? sorting_param : default_sorting
  end

  def default_sorting
		:descend_by_popularity
	end

  def allowed_sortings
    [:descend_by_popularity, :ascend_by_name, :descend_by_name, :descend_by_master_price, :ascend_by_master_price, :descend_by_created_at]
  end


end
