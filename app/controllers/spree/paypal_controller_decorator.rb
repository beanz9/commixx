Spree::PaypalController.class_eval do

  def confirm
      order = current_order || raise(ActiveRecord::RecordNotFound)
      order.payments.create!({
        source: Spree::PaypalExpressCheckout.create({
          token: params[:token],
          payer_id: params[:PayerID]
        }),
        amount: order.total,
        payment_method: payment_method
      })
      order.next
      if order.complete?
        flash.notice = Spree.t(:order_processed_successfully)
        flash[:order_completed] = true
        session[:order_id] = nil

        order.line_items.each do |line_item|
          CollectionProfit.create({
              spree_order_id: order.id,
              spree_line_item_id: line_item.id,
              collection_owner_id: line_item.collection_owner_id,
              user_id: spree_current_user.id,
              profit: line_item.price * line_item.quantity * 0.1
            }) if line_item.collection_owner_id
        end
        redirect_to completion_route(order)
      else
        redirect_to checkout_state_path(order.state)
      end
    end


end
