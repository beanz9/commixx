Spree::ProductsController.class_eval do

	helper_method :sorting_param
  alias_method :old_index, :index



	def show
		@variants = @product.variants_including_master.active(current_currency).includes([:option_values, :images])
      	@product_properties = @product.product_properties.includes(:property)
      	@taxon = Spree::Taxon.find(params[:taxon_id]) if params[:taxon_id]
      	@taxonomies = Spree::Taxonomy.includes(root: :children)

      	@review = Spree::Review.new(product: @product)
    	# authorize! :create, @review

	end

	def index
    old_index
		@products = @products.where(is_open: true)
    @products = @products.send(sorting_scope)
  end

  def sorting_param
    params[:sorting].try(:to_sym) || default_sorting
  end

  private

  def sorting_scope
    allowed_sortings.include?(sorting_param) ? sorting_param : default_sorting
  end

	def default_sorting
		:descend_by_popularity
	end

  def allowed_sortings
    [:descend_by_popularity, :ascend_by_name, :descend_by_name, :descend_by_master_price, :ascend_by_master_price, :descend_by_created_at]
  end
end
