class Spree::Admin::FeaturedProductsController < Spree::Admin::ProductsController
  #  helper 'spree/products'
  #  helper_method :clone_object_url
  #
  def index
    session[:return_to] = request.url
    @taxonomies = Spree::Taxonomy.includes(root: :children)
    respond_with(@collection)
  end

  def collection
    return @collection if @collection.present?
    params[:q] ||= {}
    params[:q][:deleted_at_null] ||= "1"

    params[:q][:s] ||= "name asc"

    taxon = Spree::Taxon.find(params[:taxon]) if params[:taxon]

    if taxon
      @collection = super.in_taxon(taxon).only_featured
    else
      @collection = super.only_featured
    end

    if params[:q].delete(:deleted_at_null) == '0'
      @collection = @collection.with_deleted
    end
    # @search needs to be defined as this is passed to search_form_for
    @search = @collection.ransack(params[:q])
    @collection = @search.result.
          distinct_by_product_ids(params[:q][:s]).
          includes(product_includes).
          page(params[:page]).
          per(params[:per_page] || Spree::Config[:admin_products_per_page])

    @collection
  end

  

end
