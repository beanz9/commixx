class Spree::Admin::SiteUrlsController < Spree::Admin::BaseController

  def new
    @site = Site.find(params[:site_id])
    @site_url = SiteUrl.new
    @site_urls = SiteUrl.where(:site_id => params[:site_id]).order('created_at DESC')

  end

  def create

    @site = Site.find(params[:site_id])
    @site_url = SiteUrl.new(site_url_params)
    @site_url.site_id = @site.id

    respond_to do |format|
      if @site_url.save
        format.html { redirect_to "/admin/sites/#{@site.id}/site_urls/new", notice: 'Site url was successfully created.' }
      else
        @site_urls = SiteUrl.where(:site_id => params[:site_id]).order('created_at DESC')
        format.html { render :new }
      end
    end

  end

  def destroy
    @site = Site.find(params[:site_id])
    @site_url = SiteUrl.find(params[:id])
    @site_url.destroy

    respond_to do |format|
      format.html { redirect_to "/admin/sites/#{@site.id}/site_urls/new", notice: 'Site url was successfully deleted.' }
    end
  end

  private

  def site_url_params
    params.require(:site_url).permit(:taxon_id, :url)
  end
end
