class Spree::Admin::SitesController < Spree::Admin::BaseController

  before_action :set_site, only: [:edit, :update, :destroy]

  def index
    @sites = Site.all
  end

  def new
    @site = Site.new
  end

  def create
    @site = Site.new(site_params)

    respond_to do |format|
      if @site.save
        format.html { redirect_to '/admin/sites', notice: 'Site was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def edit

  end




  def update
    respond_to do |format|
      if @site.update(site_params)
        format.html { redirect_to '/admin/sites', notice: 'Site was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @site.destroy
    respond_to do |format|
      format.html { redirect_to '/admin/sites', notice: 'Site was successfully updated.' }
    end
  end

  def scrap
    ScrapingJob.perform_later params[:id]

    respond_to do |format|
      format.html { redirect_to '/admin/scrap_histories' }
    end
  end


  private

  def set_site
    @site = Site.find(params[:id])
  end

  def site_params
    params.require(:site).permit(:name, :url)
  end

end
