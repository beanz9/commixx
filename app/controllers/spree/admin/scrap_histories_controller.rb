class Spree::Admin::ScrapHistoriesController < Spree::Admin::BaseController
  def index
    @scrap_histories = ScrapHistory.includes(:site).order('created_at DESC')
  end
end
