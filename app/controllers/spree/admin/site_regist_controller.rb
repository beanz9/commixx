class Spree::Admin::SiteRegistController < Spree::Admin::BaseController
  def index
    @site_regist = SiteRegist.order('created_at DESC')
  end
end
