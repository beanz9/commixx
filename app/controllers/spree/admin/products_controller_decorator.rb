Spree::Admin::ProductsController.class_eval do
	def related
		load_resource
		@relation_types = Spree::Product.relation_types
	end


	# def collection
	# 	p ">>>>>>>>>>>>"
	# 	p ">>>>>>>>>>>>"
	# 	p ">>>>>>>>>>>>"
	# 	p ">>>>>>>>>>>>"
	#
  #   return @collection if @collection.present?
	#
  #   params[:q] ||= {}
  #   params[:q][:deleted_at_null] ||= "1"
	# 	# params[:q][:scrap_product] ||= "1"
	#
  #   params[:q][:s] ||= "name asc"
	#
  #   # taxon = Spree::Taxon.find(params[:q][:taxon]) if params[:q][:taxon]
	#
	# 	@collection = super
	#
  #   if params[:q][:taxon]
	# 		taxon = Spree::Taxon.where(:id => params[:q][:taxon])
  #     @collection = @collection.in_taxon(taxon[0]) if taxon.count > 0
  #   end
	#
	#
	#
	#
  #   if params[:q].delete(:deleted_at_null) == '0'
  #     @collection = @collection.with_deleted
  #   end
  #   # @search needs to be defined as this is passed to search_form_for
  #   @search = @collection.ransack(params[:q])
  #   @collection = @search.result.
  #         distinct_by_product_ids(params[:q][:s]).
  #         includes(product_includes).
  #         page(params[:page]).
  #         per(params[:per_page] || Spree::Config[:admin_products_per_page])
	#
  #   @collection
  # end

	def collection
    return @collection if @collection.present?
    params[:q] ||= {}
    params[:q][:deleted_at_null] ||= "1"
		params[:q][:scrap_product] ||= "1"

    params[:q][:s] ||= "name asc"
    @collection = super

		taxon = Spree::Taxon.where(:id => params[:q][:taxon]).first if params[:q][:taxon]

    # Don't delete params[:q][:deleted_at_null] here because it is used in view to check the
    # checkbox for 'q[deleted_at_null]'. This also messed with pagination when deleted_at_null is checked.
    if params[:q][:deleted_at_null] == '0'
      @collection = @collection.with_deleted
    end

		if params[:q][:taxon]
			taxon = Spree::Taxon.where(:id => params[:q][:taxon])
      @collection = @collection.in_taxon(taxon[0]) if taxon.count > 0
    end

		if params[:q][:scrap_product] == '1'
			@collection = @collection.where(:is_scrap => true)
		end

    # @search needs to be defined as this is passed to search_form_for
    # Temporarily remove params[:q][:deleted_at_null] from params[:q] to ransack products.
    # This is to include all products and not just deleted products.
    @search = @collection.ransack(params[:q].reject { |k, _v| k.to_s == 'deleted_at_null' })
    @collection = @search.result.
          distinct_by_product_ids(params[:q][:s]).
          includes(product_includes).
          page(params[:page]).
          per(params[:per_page] || Spree::Config[:admin_products_per_page])
    @collection
  end
end
