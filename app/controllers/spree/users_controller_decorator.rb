Spree::UsersController.class_eval do

  alias_method :old_show, :show

  def show
    @collection_items = CollectionItem.where(:user_id => spree_current_user.id)
    old_show
  end

  def collections
    @collection_items = CollectionItem.where(:user_id => spree_current_user.id)
      .order("created_at DESC").page(params[:page]).per(10)
  end

  def collection_delete
    @collection_item = CollectionItem.find(params[:id])
    @collection_item.destroy

  end

  def profits
    @profits = CollectionProfit.where(:collection_owner_id => spree_current_user.id).order("created_at DESC")
      .page(params[:page]).per(10)
  end

end
