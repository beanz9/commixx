class CollectionsController < Spree::StoreController

  def index
    @collections = Collection.all
  end

  def show
    curr_page = params[:page] || 1
    @collection = Collection.find(params[:id])
    @collection_items = CollectionItem.where(:collection_id => params[:id]).page(curr_page).per(12)
  end

  def create
    collection = Collection.where(:user_id => spree_current_user.id).first

    collection = Collection.create(:user_id => spree_current_user.id) unless collection

    collection_item = CollectionItem.where(:product_id => params[:id], :collection_id => collection.id)

    if collection_item.count > 0
      @is_error = false
      @is_dup = true
    else
      collection_item = CollectionItem.new
      collection_item.user_id = spree_current_user.id
      collection_item.product_id = params[:id]
      collection_item.collection_id = collection.id

      if collection_item.save
        @is_error = false
        @is_dup = false
      else
        @is_error = true
        @is_dup = false
      end
    end
  end

end
