class SiteRegistController < Spree::StoreController
  def index
  end

  def new
    @site_regist = SiteRegist.new
  end

  def create
    @site_regist = SiteRegist.new(article_params)

    respond_to do |format|
      if @site_regist.save
        format.html { redirect_to '/site_regist', notice: 'Article was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  private

  def article_params
    params.require(:site_regist).permit(:email, :url, :description)
  end
end
