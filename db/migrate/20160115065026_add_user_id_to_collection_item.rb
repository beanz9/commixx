class AddUserIdToCollectionItem < ActiveRecord::Migration
  def change
    add_reference :collection_items, :spree_user, index: true, foreign_key: true
  end
end
