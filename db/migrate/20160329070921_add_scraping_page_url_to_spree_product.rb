class AddScrapingPageUrlToSpreeProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :scraping_page_url, :text
  end
end
