class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      # t.references :product, index: true, foreign_key: true
      t.references :spree_user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
