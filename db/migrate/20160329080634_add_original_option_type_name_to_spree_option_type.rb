class AddOriginalOptionTypeNameToSpreeOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_types, :original_option_type_name, :string
  end
end
