class AddCollectionToTaxonomy < ActiveRecord::Migration
  def change
    add_column :spree_taxonomies, :collection, :boolean, :default => false, :null => false
    add_column :spree_taxonomies, :featured_collection, :boolean, :default => false, :null => false
    add_column :spree_taxonomies, :collection_description, :text
  end
end
