class AddIsOpenToSpreeProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :is_open, :boolean
  end
end
