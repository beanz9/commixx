class AddOriginalOptionNameToSpreeOptionValue < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :original_option_name, :string
  end
end
