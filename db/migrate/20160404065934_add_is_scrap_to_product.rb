class AddIsScrapToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :is_scrap, :boolean, :default => false
  end
end
