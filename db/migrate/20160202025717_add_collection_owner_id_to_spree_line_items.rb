class AddCollectionOwnerIdToSpreeLineItems < ActiveRecord::Migration
  def change
    add_column :spree_line_items, :collection_owner_id, :integer
  end
end
