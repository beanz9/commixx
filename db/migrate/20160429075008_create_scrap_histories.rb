class CreateScrapHistories < ActiveRecord::Migration
  def change
    create_table :scrap_histories do |t|
      t.references :site, index: true, foreign_key: true
      t.integer :result_status
      t.text :result_desc
      t.timestamp :start_date
      t.timestamp :end_date

      t.timestamps null: false
    end
  end
end
