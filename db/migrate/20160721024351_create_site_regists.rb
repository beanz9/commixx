class CreateSiteRegists < ActiveRecord::Migration
  def change
    create_table :site_regists do |t|
      t.string :email
      t.string :url
      t.text :description
      t.boolean :is_confirm

      t.timestamps null: false
    end
  end
end
