class CreateSiteUrls < ActiveRecord::Migration
  def change
    create_table :site_urls do |t|
      t.references :site, index: true, foreign_key: true
      t.string :url
      t.integer :taxon_id

      t.timestamps null: false
    end
  end
end
