class CreateCollectionProfits < ActiveRecord::Migration
  def change
    create_table :collection_profits do |t|
      t.integer :spree_order_id
      t.integer :spree_line_item_id
      t.integer :collection_owner_id
      t.integer :user_id
      t.decimal :profit

      t.timestamps null: false
    end
  end
end
