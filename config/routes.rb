require 'sidekiq/web'

Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  mount Spree::Core::Engine, :at => '/'

  resources :snippets
  root to: "snippets#new"
  mount Sidekiq::Web, at: "/sidekiq"

  Spree::Core::Engine.add_routes do
    namespace :admin do
      get '/featured_products', :to => 'featured_products#index'
      put '/featured_products/:id', :to => 'featured_products#update', :as => 'featured_product'

      get '/site_regist', :to => 'site_regist#index'

      resources :sites do
        member do
          post 'scrap'

        end
        resources :site_urls

      end

      resources :scrap_histories

    end

    get '/account/collections', :to => 'users#collections', :as => 'user_collections'
    delete '/account/collections/:id', :to => 'users#collection_delete'
    get '/account/profits', :to => 'users#profits'
  end

  get '/collections', :to => 'collections#index'
  get '/collections/:id', :to => 'collections#show', :as => 'collection_show'
  post '/collections', :to => 'collections#create'


  get '/site_regist', :to => 'site_regist#index'
  get '/site_regist/new', :to => 'site_regist#new'
  post '/site_regist', :to => 'site_regist#create'



          # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
