require "#{Rails.root}/lib/jobs/scraping_detail"

class Scraping

  def self.scrap(site_id)
    @default_url = Site.find(site_id)

    category_list  = SiteUrl.where(:site_id => @default_url.id)
    # @default_url = 'http://zokomisj.cafe24.com/'
    # category_list = ['http://zokomisj.cafe24.com/product/list.html?cate_no=7']


    @agent = Mechanize.new
    @is_tanslate = false
    @translator = BingTranslator.new('694cf8d6bf254e2da17a2a4160ecc2d5')


    category_list.each do |category|

      page = @agent.get(category.url)


      get_product_info(page, category.taxon_id)

      nav_links = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging']/ol/li/a/@href")

      nav_links.each_with_index do |link, i|
        if i > 0
          product_page = @agent.get(@default_url.url + '/product/list.html' + link)
          get_product_info(product_page, category.taxon_id)
        end

        # p link.value
      end
    end




  end

  def self.init
  end

  def self.get_product_info(page, taxon_id)


    products = page.parser.xpath("//li[@class='item xans-record-']/div/a[1]/@href")

    suppress(Exception) do
      products.each do |product|
        product_page = @agent.get(@default_url.url + product.value)
        p '****************************'
        p @default_url.url + product.value
        p "수집중...."
        # title = product_page.parser.xpath("//div[@class='xans-element- xans-product xans-product-detaildesign']/table/tbody/tr/td/span")[0].text
        # title = product_page.search('meta[property="og:title"]')[-1]
        # title = product_page.at('meta[property="og:title"]')[:content]
        # p translator(title)

          ScrapingDetail.get_detail(@default_url.url + product.value, taxon_id)

        p "수집완료"
        p '****************************'
      end
    end
  end

  def self.translator(char)
    return @translator.translate(char, :from => 'ko', :to => 'en') if @is_tanslate
    char
  end
end
