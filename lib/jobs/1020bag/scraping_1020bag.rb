require "#{Rails.root}/lib/jobs/1020bag/scraping_1020bag_detail"

class Scraping1020bag
  def self.scrap(taxon_id)
    login()

    category_urls = [
      # { url: 'http://1020bag.com/product/list.html?cate_no=80', id: 16 }, #woman
      # { url: 'http://1020bag.com/product/list.html?cate_no=82', id: 17 }, #travel
      # { url: 'http://1020bag.com/product/list.html?cate_no=83', id: 18 }, #casual
      { url: 'http://1020bag.com/product/list.html?cate_no=84', id: 19 }, #backpack
      # { url: 'http://1020bag.com/product/list.html?cate_no=87', id: 20 }, #climbing
      # { url: 'http://1020bag.com/product/list.html?cate_no=86', id: 21 } #business
    ]

    # category_urls = [
    #   { url: 'http://1020bag.com/product/list.html?cate_no=80', id: 42 }, #woman
    #   { url: 'http://1020bag.com/product/list.html?cate_no=82', id: 43 }, #travel
    #   { url: 'http://1020bag.com/product/list.html?cate_no=83', id: 44 }, #casual
    #   { url: 'http://1020bag.com/product/list.html?cate_no=84', id: 45 }, #backpack
    #   { url: 'http://1020bag.com/product/list.html?cate_no=87', id: 46 }, #climbing
    #   { url: 'http://1020bag.com/product/list.html?cate_no=86', id: 47 } #business
    # ]



      category_urls.each do |category|
        suppress(Exception) do
          page = @agent.get(category[:url])

          last_nav_link = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging']/p/a/@href")[-1]
          last_page_num = last_nav_link.value.split('page=')[1]

          if last_nav_link.value == '#none'
            get_product_info(page, category[:id], @agent)
          else
            (1..last_page_num.to_i).each do |i|
              param = last_nav_link.value.split('page=')[0] + 'page=' + i.to_s
              product_page = @agent.get('http://1020bag.com/product/list.html' + param)
              get_product_info(product_page, category[:id], @agent)
            end
          end
        end
      end


    # page_url = 'http://1020bag.com/product/list.html?cate_no=28'
    # page = @agent.get(page_url)
    #
    # # nav_links = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging']/ol/li/a/@href")
    # last_nav_link = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging']/p/a/@href")[-1]
    # last_page_num = last_nav_link.value.split('page=')[1]
    #
    # if last_nav_link.value == '#none'
    #   get_product_info(page, 5, @agent)
    # else
    #   (1..last_page_num.to_i).each do |i|
    #     param = last_nav_link.value.split('page=')[0] + 'page=' + i.to_s
    #     product_page = @agent.get('http://1020bag.com/product/list.html' + param)
    #     get_product_info(product_page, 3, @agent)
    #   end
    # end



    # nav_links.each_with_index do |link, i|
    #   if i > 0
    #     product_page = @agent.get('1020bag.com/product/list.html' + link)
    #     get_product_info(product_page, 5, @agent)
    #   end
    #
    # end

    # get_product_info(4,3,@agent)
    # product_page = @agent.get('http://1020bag.com/product/detail.html?product_no=3127&cate_no=1&display_group=4')
    # p product_page.parser.xpath("//div[@id='prdInfo']/h3")
  end



  def self.login
    @agent = Mechanize.new
    home_page = @agent.get('http://1020bag.com/')

    if home_page.parser.xpath("//div[@id='log']/div/ul/a/@href")[0].value == '/member/login.html'
      login_page = @agent.get("http://1020bag.com/member/login.html")
      login_form = login_page.forms[1]

      id_field = login_form.field_with(name: "member_id")
      password_field = login_form.field_with(name: "member_passwd")

      id_field.value = 'beanz9'
      password_field.value = 'quusome0912'

      login_form.submit
    end
  end

  # def self.get_product_info(page, taxon_id, agent)
  #   ScrapingDetail.get_detail(page, taxon_id, agent)
  # end

  def self.get_product_info(page, taxon_id, agent)


    products = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-listnormal ga09list']/ul/li/a/@href")


    products.each do |product|
      p "========"
      # product_page = @agent.get('http://1020bag.com' + product.value)
      # title = product_page.parser.xpath("//div[@class='xans-element- xans-product xans-product-detaildesign']/table/tbody/tr/td/span")[0].text
      # p translator(title)
      Scraping1020bagDetail.get_detail('http://1020bag.com' + product.value, taxon_id, agent)
    end
  end
end
