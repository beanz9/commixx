class Scraping1020bagDetail
  def self.get_detail(product_url=nil, taxon_id=nil, agent)
    @agent = agent
    @translator = BingTranslator.new('beanz9', 'QIczPcEuqaoYX61t6vBMU9U4XYJtStzCiwZkhBxRFHg=')
    # @agent = Mechanize.new

    # product_url = 'http://1020bag.com/product/detail.html?product_no=2430&cate_no=1&display_group=4'
    # product_url = 'http://1020bag.com/product/detail.html?product_no=2574&cate_no=74&display_group=1'
    # product_url = 'http://1020bag.com/product/detail.html?product_no=2430&cate_no=1&display_group=4'
    # product_url = 'http://1020bag.com/product/detail.html?product_no=2509&cate_no=28&display_group=1'
    product_page = @agent.get(product_url)
    p product_url


    image_url = @agent.page.at('meta[property="og:image"]')[:content]
    title = product_page.parser.xpath("//div[@id='prdInfo']/h3").text.strip
    price = product_page.parser.xpath("//strong[@id='span_product_price_text']").text
    content = product_page.parser.xpath("//div[@id='detailArea']/img")

    product = Spree::Product.new
    product.name = @translator.translate(title, :from => 'ko', :to => 'en')
    product.description = content.to_html
    product.shipping_category_id = 1
    product.available_on = Time.now
    product.price = price.gsub(/[^\d]/, '').to_f / 1164
    product.is_open = true
    product.is_scrap = true
    product.scraping_page_url = product_url
    product.domain = '1020bag.com'

    product.save

    Spree::Classification.create(product_id: product.id, taxon_id: taxon_id)

    uri = URI.parse(image_url)
    product_image = Spree::Image.new
    product_image.attachment = uri.open
    product_image.attachment_file_name = File.basename(uri.path)
    product_image.viewable_id = product.master.id
    product_image.viewable_type = "Spree::Variant"
    product_image.position = 0

    product_image.save

    option_type = product_page.parser.xpath("//tbody[@class='xans-element- xans-product xans-product-option xans-record-']/tr/th")

    option_values = product_page.parser.xpath("//tbody[@class='xans-element- xans-product xans-product-option xans-record-']/tr/td/select")
    # option_value[0].children[1].attr('value')
    option_arr = []

    option_type.each_with_index do |type, i|
      spree_option_type = Spree::OptionType.where(:original_option_type_name => type.text).first

      unless spree_option_type
        option_type_text = @translator.translate(type.text, :from => 'ko', :to => 'en')
        temp_option_type = Spree::OptionType.new
        temp_option_type.name = option_type_text
        temp_option_type.presentation = option_type_text
        temp_option_type.original_option_type_name = type.text
        temp_option_type.save
        spree_option_type = temp_option_type

      end

      spree_product_option_type = Spree::ProductOptionType.new
      spree_product_option_type.product_id = product.id
      spree_product_option_type.option_type_id = spree_option_type.id
      spree_product_option_type.save

      arr = []



      option_values[i].children.each do |v|
          value = v.attr('value')

          if value != nil && value != '*' && value != '**'

            option_name = v.text.split('--')[0]
            option_name = option_name.split('/')[0]

            option_value = Spree::OptionValue.new
            option_value_chk = Spree::OptionValue.where(:original_option_name => option_name,
              :option_type_id => spree_option_type.id).first



            trans_value = ""
            if option_value_chk
              trans_value = option_value_chk.name
            else
              trans_value = @translator.translate(option_name, :from => 'ko', :to => 'en')
            end

            option_value.name = trans_value
            option_value.presentation = trans_value
            option_value.option_type_id = spree_option_type.id
            option_value.original_option_name = option_name
            option_value.save unless option_value_chk

            arr << Spree::OptionValue.where(:original_option_name => option_name,
              :option_type_id => spree_option_type.id).first.id


          end
      end

      option_arr << arr.uniq
    end

    if option_arr.size > 0
      cartesian_arr = option_arr[1..-1].inject(option_arr[0]) {
        |m, v| m = m.product(v).map(&:flatten)
      }


      cartesian_arr.each do |p_arr|
        variant = Spree::Variant.new
        variant.product_id = product.id
        variant.cost_currency = "USD"
        variant.stock_items_count = 1
        variant.option_values = Spree::OptionValue.where(id: p_arr)
        variant.save
      end
    end

  end


end
