require "#{Rails.root}/lib/jobs/styledome/scraping_styledome_detail"

class ScrapingStyledome
  def self.scrap(site_id)
    login()

    category_urls = [
      { url: 'http://www.styledome.co.kr/product/list.html?cate_no=27', id: 14 }, #OUTER
      { url: 'http://www.styledome.co.kr/product/list.html?cate_no=28', id: 10 }, #TOP
      { url: 'http://www.styledome.co.kr/product/list.html?cate_no=30', id: 11 }, #BOTTOM
      { url: 'http://www.styledome.co.kr/product/list.html?cate_no=32', id: 12 }, #SKIRT
      { url: 'http://www.styledome.co.kr/product/list.html?cate_no=29', id: 13 }, #DRESS
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=33', id: 64 }, #TRAINING
      { url: 'http://www.styledome.co.kr/product/list.html?cate_no=49', id: 4 } #ACCESSORIES

      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=772', id: 6 }, #Tee
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=773', id: 6 }, #Sleeveless
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=774', id: 9 }, #Sweater
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=775', id: 6 }, #Shirt
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=776', id: 11 }, #Pants
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=777', id: 32 }, #Leggings
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=780', id: 33 }, #Shorts
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=32', id: 13 }, #SKIRT
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=29', id: 17 }, #DRESS
      # { url: 'http://www.styledome.co.kr/product/list.html?cate_no=27', id: 5 }  #OUTER
    ]

    category_urls.each do |category|
      page = @agent.get(category[:url])

      suppress(Exception) do
        last_nav_link = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging']/p/a/@href")[-1]
        last_page_num = last_nav_link.value.split('page=')[1]

        if last_nav_link.value == '#none'
          get_product_info(page, category[:id], @agent)
        else
          (1..last_page_num.to_i).each do |i|
            param = last_nav_link.value.split('page=')[0] + 'page=' + i.to_s
            product_page = @agent.get('http://www.styledome.co.kr/product/list.html' + param)
            get_product_info(product_page, category[:id], @agent)
          end
        end
      end

    end

  end

  def self.login
    @agent = Mechanize.new
    home_page = @agent.get('http://www.styledome.co.kr/')

    if home_page.parser.xpath("//div[@id='header']/div[@class='section2']/div/ul/li/a/@href")[0].value == '/member/login.html'
      login_page = @agent.get("http://www.styledome.co.kr/member/login.html")
      login_form = login_page.forms[1]

      id_field = login_form.field_with(name: "member_id")
      password_field = login_form.field_with(name: "member_passwd")

      id_field.value = 'beanz9'
      password_field.value = 'quusome0912'

      login_form.submit
    end
  end

  def self.get_product_info(page, taxon_id, agent)


    products = page.parser.xpath("//ul[@class='prdList column4']/li[@class='item xans-record-']/div/a/@href")


    products.each do |product|
      p "========"
      # product_page = @agent.get('http://1020bag.com' + product.value)
      # title = product_page.parser.xpath("//div[@class='xans-element- xans-product xans-product-detaildesign']/table/tbody/tr/td/span")[0].text
      # p translator(title)
      ScrapingStyledomeDetail.get_detail('http://www.styledome.co.kr' + product.value, taxon_id, agent)
    end
  end
end
