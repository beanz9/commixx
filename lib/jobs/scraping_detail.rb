# encoding: utf-8

class ScrapingDetail

  def self.get_detail(product_url=nil, taxon_id)
    @translator = BingTranslator.new('694cf8d6bf254e2da17a2a4160ecc2d5')
    @agent = Mechanize.new

    # product_url = 'http://zokomisj.cafe24.com/product/detail.html?product_no=1652&cate_no=4&display_group=1'
    # product_url = 'http://zokomisj.cafe24.com/product/detail.html?product_no=1776&cate_no=61&display_group=1'
    # @product_page = 'http://zokomisj.cafe24.com/product/detail.html?product_no=1902&cate_no=1&display_group=3'
    # product_url = 'http://zokomisj.cafe24.com/product/detail.html?product_no=1717&cate_no=1&display_group=2'
    # product_url = 'http://zokomisj.cafe24.com/product/detail.html?product_no=1867&cate_no=4&display_group=1'
    # product_url = 'http://www.stylenanda.com/product/detail.html?product_no=219053&cate_no=50&display_group=2'
    # product_url = 'http://www.66girls.co.kr/product/detail.html?product_no=56876&cate_no=1&display_group=2'
    # product_url = 'http://www.mixxmix.com/product/detail.html?product_no=46916&cate_no=837&display_group=1'
    # product_url = 'http://www.miamasvin.co.kr/product/detail.html?product_no=42307&cate_no=1&display_group=3'
    # product_url = 'http://www.meosidda.com/product/detail.html?product_no=23809&cate_no=1&display_group=2'
    # product_url = 'http://www.chuu.co.kr/product/detail.html?product_no=19152&cate_no=1138&display_group=1'
    # product_url = 'http://www.ggsing.com/product/detail.html?product_no=34885&cate_no=1&display_group=2'
    # product_url = 'http://www.dabagirl.co.kr/product/detail.html?product_no=27350&cate_no=1&display_group=5'
    # product_url = 'http://www.imvely.com/product/detail.html?product_no=12164&cate_no=41&display_group=2'
    # product_url = 'http://www.bluepops.co.kr/product/detail.html?product_no=29686&cate_no=38&display_group=1'
    # product_url = 'http://www.darkvictory.co.kr/product/detail.html?product_no=23020&cate_no=1&display_group=2'
    # product_url = 'http://www.bagazimuri.com/product/detail.html?product_no=92712&cate_no=1&display_group=3'
    # product_url = 'http://justoshop.com/product/detail.html?product_no=92&cate_no=24&display_group=1'
    # product_url = 'http://leesle.com/product/detail.html?product_no=256&cate_no=1&display_group=3'
    # product_url = 'http://www.2dello.com/product/detail.html?product_no=452&cate_no=55&display_group=1'
    # product_url = 'http://www.instylefit.com/product/detail.html?product_no=24838&cate_no=1&display_group=8'
    # product_url = 'http://www.indibrand.co.kr/product/detail.html?product_no=6854&cate_no=1&display_group=3'
    # product_url = 'http://romp.com/product/detail.html?product_no=4878&cate_no=1&display_group=2'
    # product_url = 'http://www.daycomma.com/product/detail.html?product_no=332&cate_no=1&display_group=3'
    # product_url = 'http://www.pinksecret.co.kr/product/detail.html?product_no=17031&cate_no=1&display_group=3'
    # product_url = 'http://www.shoesdeblanc.com/product/detail.html?product_no=312&cate_no=1&display_group=3'
    # product_url = 'http://www.hutsandbay.com/product/detail.html?product_no=511&cate_no=44&display_group=1'
    # product_url = 'http://www.callmesue.com/product/detail.html?product_no=2423&cate_no=1&display_group=2'
    # product_url = 'http://www.ojane.co.kr/product/detail.html?product_no=3357&cate_no=1&display_group=2'
    # product_url = 'http://www.bebedepino.com/product/detail.html?product_no=375&cate_no=31&display_group=1'
    # product_url = 'http://www.a-mont.com/product/detail.html?product_no=1027&cate_no=145&display_group=1'
    # product_url = 'http://zokomisj.cafe24.com/product/detail.html?product_no=1775&cate_no=7&display_group=1'
    product_url_partial = product_url.split('/')

    domain = product_url_partial[2]

    product_page = @agent.get(product_url)

    product_html_path = "//div[@class='xans-element- xans-product xans-product-detaildesign']/table/tbody/tr/td/span"


    # title = product_page.parser.xpath(product_html_path)[0].text
    # price = product_page.parser.xpath(product_html_path)[1].text
    option_name = product_page.parser.xpath("//div[@class='infoArea']/table/tbody/tr/th")


    content = product_page.parser.xpath("//div[@class='cont']")
    # image = product_page.parser.xpath("//div[@class='keyImg']/img/@src").value

    image_url = @agent.page.at('meta[property="og:image"]')[:content]
    image_urls = @agent.page.search('meta[property="og:image"]')

    # image_url = @agent.page.search("//div[@class='keyImg']")
    #
    # unless image_url.empty?
    #   image_url = image_url.at('img')['src']
    # else
    #   image_url = @agent.page.search("//p[@class='keyImg']").at('img')['src']
    # end
    scripts = product_page.parser.xpath("//script")
    script = ""
    scripts.each do |str|
      if (str.to_s).include? "var mobileWeb"
        script = str
      end
    end

    split_str = script.to_s.include? "var option_stock_data = '"

    # p "split_str"
    # p split_str

    if split_str
      script1 = script.to_s.split("var option_stock_data = '")[1]
      # variable = script.to_s.match(/var\s+option_stock_data\s+=\s+(.*?);/m)[1]

      script2 = script1.split("';var stock_manage")[0]
    # p script2
      # p script


      # json = JSON.parse(script2.to_s.gsub('\\', ''))
      # p script2.gsub(/\\"/,'"').gsub(/\\\\/, '\\')

      json = JSON.parse(script2.gsub(/\\"/,'"').gsub(/\\\\/, '\\'))




      option_name_mapper = script.to_s.split("var option_name_mapper = '")[1]
      option_name_mapper = option_name_mapper.split("';")[0]
    end



    # title = product_page.at('meta[property="og:title"]')[:content]

    title = product_page.search('meta[property="og:title"]')[-1][:content]

    if title != nil
      title = title[0, 50]
    else
      title = script.to_s.split("var product_name = '")[1]
      title = title.split("';")[0]
    end

    price = script.to_s.split("product_price = '")[1]
    price = price.split("';")[0]


    p "상품명 : #{title}"
    # p title
    # p price.gsub(/[^\d]/, '')
    #
    #
    # p content.to_html
    # p image_url


    if content.size == 0
      content = product_page.parser.xpath("//div[@id='detailArea']")
    end

    content.search('img').each do |node|
      src_url = node.attributes["src"].value

      unless src_url.start_with?("http")
        if src_url.start_with?("//")
          node.attributes["src"].value = "http:" + src_url
        else
          node.attributes["src"].value = "http://" + domain + src_url
        end
      end

    end


    product = Spree::Product.new
    product.name = @translator.translate(title, :from => 'ko', :to => 'en')
    product.description = content.to_html
    product.shipping_category_id = 1
    product.available_on = Time.now
    product.price = price.gsub(/[^\d]/, '').to_f / 1164
    product.is_open = true
    product.is_scrap = true
    product.scraping_page_url = product_url
    product.domain = domain

    product.save




    Spree::Classification.create(product_id: product.id, taxon_id: taxon_id)


    image_urls.each_with_index do |image, i|
      image_url = image[:content]
      unless image_url.start_with? "http"
        image_url = "http:" + image_url
      end

      unless image_url.include? "tiny"
        # p ">>>>>>>>>>>"
        # p image_url
        # p ">>>>>>>>>>>"
        if image_url.include? "jpg" or image_url.include? "jpeg" or image_url.include? "png"
          uri = URI.parse(image_url)
          product_image = Spree::Image.new
          product_image.attachment = uri.open
          product_image.attachment_file_name = File.basename(uri.path)
          product_image.viewable_id = product.master.id
          product_image.viewable_type = "Spree::Variant"
          product_image.position = i

          product_image.save
        end
      end
    end




    # uri = URI.parse(image_url)
    # product_image = Spree::Image.new
    # product_image.attachment = uri.open
    # product_image.attachment_file_name = File.basename(uri.path)
    # product_image.viewable_id = product.master.id
    # product_image.viewable_type = "Spree::Variant"
    # product_image.position = 1
    #
    # product_image.save

    # p product_image.errors


    if split_str
      option_arr = []
      option_name_mapper.split('#$%').each_with_index do |option_type, i|
      # option_name.each_with_index do |option_type, i|
          # p option_type.text
          # option_type_text = @translator.translate(option_type.text, :from => 'ko', :to => 'en')
          spree_option_type = Spree::OptionType.where(:original_option_type_name => option_type).first

          unless spree_option_type
            option_type_text = @translator.translate(option_type, :from => 'ko', :to => 'en')
            temp_option_type = Spree::OptionType.new
            temp_option_type.name = option_type_text
            temp_option_type.presentation = option_type_text
            temp_option_type.original_option_type_name = option_type
            temp_option_type.save
            spree_option_type = temp_option_type

          end

          spree_product_option_type = Spree::ProductOptionType.new
          spree_product_option_type.product_id = product.id
          spree_product_option_type.option_type_id = spree_option_type.id
          spree_product_option_type.save

          arr = []
          json.each do |key, value|


            # p value["option_name"]
            # p value["option_value"]



            value["option_name"].split('#$%').each_with_index do |name, i|
              # arr2 = []
              # p "((((((((((((((()))))))))))))))"
              # p value["option_name"].split('#$%')[i].gsub(/\s+/, "")



              if value["option_name"].split('#$%')[i].gsub(/\s+/, "")  == option_type
                option_value = Spree::OptionValue.new
                option_value_chk = Spree::OptionValue.where(:original_option_name => value["option_value"].split('-')[i],
                  :option_type_id => spree_option_type.id).first

                # p "option_value_chk"
                # p  option_value_chk

                trans_value = ""
                if option_value_chk
                  trans_value = option_value_chk.name
                else
                  if trans_value.is_number?
                    trans_value = value["option_value"].split('-')[i]
                  else
                    trans_value = @translator.translate(value["option_value"].split('-')[i], :from => 'ko', :to => 'en')
                  end
                end

                option_value.name = trans_value
                option_value.presentation = trans_value
                option_value.option_type_id = spree_option_type.id
                option_value.original_option_name = value["option_value"].split('-')[i]
                option_value.save unless option_value_chk
                # p option_value.errors
                #
                # p "option_value"
                # p option_value
                #
                # p "//////////////////////"
                # p value["option_value"].split('-')[i]
                # p spree_option_type
                # p spree_option_type.id
                # p product_url

                arr << Spree::OptionValue.where(:original_option_name => value["option_value"].split('-')[i],
                  :option_type_id => spree_option_type.id).first.id

                  # p trans_value

                # arr2 << option_value.id
              end


              # arr << arr2
            end

          end
          option_arr << arr.uniq


          # p spree_option_type


      end
      cartesian_arr = option_arr[1..-1].inject(option_arr[0]) {
        |m, v| m = m.product(v).map(&:flatten)
      }

      # p "================="
      # p option_arr

      cartesian_arr.each do |p_arr|
        variant = Spree::Variant.new
        variant.product_id = product.id
        variant.cost_currency = "USD"
        variant.stock_items_count = 1
        variant.option_values = Spree::OptionValue.where(id: p_arr)
        variant.save

        # p_arr.each do |c_arr|
        #   option_value_variant = Spree::OptionValuesVariant.new
        #   option_value_variant.variant_id = variant.id
        #   option_value_variant.option_value_id = c_aar
        #   option_value_variant.save
        # end
      end
    end


    # json.each do |key, value|
    #   # p value
    #   value["option_name"].split('#$%').each_with_index do |name, i|
    #     p name
    #     p value["option_value"].split('-')[i]
    #   end
    #   # p value["option_name"].split('#$%')[0]
    #   # p value["option_value"].split('-')[0]
    #   #
    #   # p value["option_name"].split('#$%')[1]
    #   # p value["option_value"].split('-')[1]
    #
    #
    # end
  end
end

class Object
  def is_number?
    self.to_f.to_s == self.to_s || self.to_i.to_s == self.to_s
  end
end
