require "#{Rails.root}/lib/jobs/scraping_detail"

class ScrapingTest

  def self.scrap(site_id)
    p ">>>>>>>>>>>"
    @default_url = Site.find(site_id)

    # category_list  = SiteUrl.where(:site_id => @default_url.id)
    @default_url = 'http://www.bluepops.co.kr'
    category_list = ['http://www.bluepops.co.kr/product/list.html?cate_no=38']


    @agent = Mechanize.new
    @is_tanslate = false
    @translator = BingTranslator.new('beanz9', 'QIczPcEuqaoYX61t6vBMU9U4XYJtStzCiwZkhBxRFHg=')


    category_list.each do |category|

      # page = @agent.get(category.url)
      page = @agent.get(category)


      # get_product_info(page, category.taxon_id)
      get_product_info(page, 4)

      nav_links = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging']/ol/li/a/@href")

      nav_links.each_with_index do |link, i|
        if i > 0
          product_page = @agent.get(@default_url.url + '/product/list.html' + link)
          get_product_info(product_page, category.taxon_id)
        end

        # p link.value
      end
    end




  end

  def self.init
  end

  def self.get_product_info(page, taxon_id)


    products = page.parser.xpath("//li[@class='item xans-record-']/div/a/@href")


    products.each do |product|
      # product_page = @agent.get(@default_url.url + product.value)
      product_page = @agent.get(@default_url + product.value)
      p '****************************'
      # p @default_url.url + product.value
      p "수집중...."
      title = product_page.parser.xpath("//div[@class='xans-element- xans-product xans-product-detaildesign']/table/tbody/tr/td/span")[0].text
      # p translator(title)
      # ScrapingDetail.get_detail(@default_url.url + product.value, taxon_id)
      ScrapingDetail.get_detail(@default_url + product.value, taxon_id)
      p "수집완료"
      p '****************************'
    end
  end

  def self.translator(char)
    return @translator.translate(char, :from => 'ko', :to => 'en') if @is_tanslate
    char
  end
end
