require "#{Rails.root}/lib/jobs/shuline/scraping_shuline_detail"

class ScrapingShuline
  def self.scrap(site_id)

    login()

    p "========"
    category_urls = [
      # { url: 'http://www.shuline.co.kr/product/list.html?cate_no=27', id: 22 },
      # { url: 'http://www.shuline.co.kr/product/list.html?cate_no=42', id: 23 },
      # { url: 'http://www.shuline.co.kr/product/list.html?cate_no=28', id: 24 },
      # { url: 'http://www.shuline.co.kr/product/list.html?cate_no=45', id: 25 },
      # { url: 'http://www.shuline.co.kr/product/list.html?cate_no=26', id: 26 },
      { url: 'http://www.shuline.co.kr/product/list.html?cate_no=43', id: 26 }


      # { url: 'http://www.shuline.co.kr/product/list.html?cate_no=27', id: 6 }
    ]

    category_urls.each do |category|

      suppress(Exception) do
        page = @agent.get(category[:url])
        last_nav_link = page.parser.xpath("//div[@class='xans-element- xans-product xans-product-normalpaging ec-base-paginate']/a/@href")[-1]

        last_page_num = last_nav_link.value.split('page=')[1]

        if last_nav_link.value == '#none'
          get_product_info(page, category[:id], @agent)
        else
          (1..last_page_num.to_i).each do |i|
            param = last_nav_link.value.split('page=')[0] + 'page=' + i.to_s
            product_page = @agent.get('http://shuline.co.kr/product/list.html' + param)
            p 'http://shuline.co.kr/product/list.html' + param
            get_product_info(product_page, category[:id], @agent)
          end
        end
      end

    end

  end

  def self.login
    @agent = Mechanize.new
    home_page = @agent.get('http://shuline.com/')

    if home_page.parser.xpath("//div[@class='xans-element- xans-layout xans-layout-statelogoff ']/a/@href")[0].value == '/member/login.html'
      login_page = @agent.get("http://shuline.co.kr/member/login.html")
      login_form = login_page.forms[1]

      id_field = login_form.field_with(name: "member_id")
      password_field = login_form.field_with(name: "member_passwd")

      id_field.value = 'beanz10'
      password_field.value = 'quusome0912'

      login_form.submit
    end
  end

  def self.get_product_info(page, taxon_id, agent)

    products = page.parser.xpath("//ul/li[@class='xans-record-']/div/a[1]/@href")

    products.each do |product|
      # product_page = @agent.get('http://1020bag.com' + product.value)
      # title = product_page.parser.xpath("//div[@class='xans-element- xans-product xans-product-detaildesign']/table/tbody/tr/td/span")[0].text
      # p translator(title)
      ScrapingShulineDetail.get_detail('http://shuline.co.kr' + product.value, taxon_id, agent)
    end
  end
end
