require "#{Rails.root}/lib/jobs/1020bag/scraping_1020bag"
require "#{Rails.root}/lib/jobs/1020bag/scraping_1020bag_detail"

namespace :scraping_1020bag do
  task :run => [:environment] do
    p "!!!!!"
    Scraping1020bag.scrap(3)
  end

  task :detail => [:environment] do
    ScrapingDetail.get_detail(4, 3)
  end
end
