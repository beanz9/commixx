require 'mechanize'
require 'cgi'

class Test

	DEFAULT_URL = 'http://welinker.co.kr'
	LIST_URL = 'http://welinker.co.kr/product/list.html'
	@agent = Mechanize.new
	page = @agent.get('http://welinker.co.kr/product/list.html?cate_no=54')

	# product_links = page.links.find_all { |l| p l.attributes }
	@count = 0

	def self.get_product_info(page)
		product_links = page.parser.xpath("//p[@class='vx122-img0']/a/@href")

		product_links.each do |l|
			product_page = @agent.get(DEFAULT_URL + l.value)
			# p product_page.parser.xpath("//tr[@class=' xans-record-']/td/span[@style='font-size:16px;color:#555555;']").text
			@count = @count + 1
			p @count
		end
	end


	get_product_info(page)


	def self.page_link(page)
		page.links.each do |link|
			if link.text == '다음 페이지'
				page_param = CGI::parse(LIST_URL + link.uri.to_s)['page']
				unless page_param.empty?
					page = @agent.get(LIST_URL + link.uri.to_s)
					get_product_info(page)
					page_link(page)
				end

			end
		end

	end

	page_link(page)



end
