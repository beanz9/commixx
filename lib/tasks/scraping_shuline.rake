require "#{Rails.root}/lib/jobs/shuline/scraping_shuline"
require "#{Rails.root}/lib/jobs/shuline/scraping_shuline_detail"

namespace :scraping_shuline do
  task :run => [:environment] do
    p ">>>>>>>"
    ScrapingShuline.scrap(4)
  end

  # task :detail => [:environment] do
  #   ScrapingDetail.get_detail(4, 3)
  # end
end
