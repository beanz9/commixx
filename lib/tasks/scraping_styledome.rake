require "#{Rails.root}/lib/jobs/styledome/scraping_styledome"
require "#{Rails.root}/lib/jobs/styledome/scraping_styledome_detail"

namespace :scraping_styledome do
  task :run => [:environment] do
    p "!!!!!"
    ScrapingStyledome.scrap(4)
  end

  task :detail => [:environment] do
    ScrapingDetail.get_detail(4, 3)
  end
end
