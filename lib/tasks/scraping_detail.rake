require "#{Rails.root}/lib/jobs/scraping"


namespace :scraping_detail do
  task :run do
    Scraping.scrap
  end
end
