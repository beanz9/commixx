require "#{Rails.root}/lib/jobs/phgmy/scraping_phgmy"
require "#{Rails.root}/lib/jobs/phgmy/scraping_phgmy_detail"

namespace :scraping_phgmy do
  task :run => [:environment] do
    p ">>>>>>>"
    ScrapingPhgmy.scrap(4)
  end

  # task :detail => [:environment] do
  #   ScrapingDetail.get_detail(4, 3)
  # end
end
