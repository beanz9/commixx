require "#{Rails.root}/lib/jobs/scraping"
require "#{Rails.root}/lib/jobs/scraping_test"
require "#{Rails.root}/lib/jobs/scraping_detail"

namespace :scraping do
  task :run => [:environment] do
    Scraping.scrap(4)
  end

  task :detail => [:environment] do
    ScrapingDetail.get_detail(4, 3)
  end

  task :test => [:environment] do
    ScrapingTest.scrap(1)
  end
end
